import 'leaflet'
import 'leaflet.markercluster'
import 'leaflet-routing-machine'

const map = L.map('map').setView([48.85697876155512, 2.34609603881836], 13);

L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  maxZoom: 19
}).addTo(map);

var markersCluster = L.markerClusterGroup({
  iconCreateFunction: function (cluster) {
    return L.divIcon({
      html: cluster.getChildCount(),
      className: 'mycluster',
      iconSize: null
    });
  },
  disableClusteringAtZoom: 18
}
);

var myRenderer = L.canvas({ padding: 0.5 })

var mode = 1

const markers = [[], [], [], [], [], [], [], [], [], []]

const markerLight = new Map()

let index = 0;

fetch('coord.json')
  .then(r => r.json())
  .then(r => {

    r.forEach(coord => {
      const marker = L.circleMarker([coord[0], coord[1]], {
        renderer: myRenderer,
        color: '#FF3B30'
      })

      markersCluster.addLayer(marker)

      markers[index].push(marker)
      markerLight.set(marker, false)

      const button = document.createElement("button")
      button.innerText = "Switch light"
      button.addEventListener("click", () => {
        switchLight(marker)
      })

      const popupDiv = document.createElement("div")
      popupDiv.classList.add("popup-content")

      const fullAddressName = document.createElement("p")
      fullAddressName.innerText = coord[2]
      fullAddressName.classList.add("popup-full-address")

      popupDiv.appendChild(fullAddressName)
      popupDiv.appendChild(button)

      marker.bindPopup(popupDiv)

      if (index == 9) {
        index = 0;
      } else {
        index++
      }
    });

    map.addLayer(markersCluster);
  })

function getDelay(index) {
  const delays = [25000, 30000, 40000, 45000, 50000, 55000, 60000, 750000, 80000, 90000]
  return delays[index]
}

for (let ii = 0; ii < markers.length; ii++) {
  const markerss = markers[ii];

  setInterval(() => {

    markerss.forEach(marker => {
      switchLight(marker)
    })
  }, getDelay(ii));
}

function switchLight(marker) {
  const isGreen = markerLight.get(marker)
  if (isGreen) {
    marker.setStyle({ color: '#FF3B30' })
  } else {
    marker.setStyle({ color: '#45D546' })
  }
  markerLight.set(marker, !isGreen)
}

const taxiIcon = L.icon({
  iconUrl: 'taxi.png',
  iconSize: [75, 52]
})

let startMarker = null
let endMarker = null

map.on("click", e => {
  if (mode === 1) {

    if (startMarker === null) {
      startMarker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
    } else if (endMarker == null) {
      endMarker = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);

      let taxiMarker = L.marker([startMarker.getLatLng().lat, startMarker.getLatLng().lng], { icon: taxiIcon }).addTo(map);

      L.Routing.control({
        waypoints: [
          startMarker.getLatLng(),
          endMarker.getLatLng()
        ],
        router: L.Routing.mapbox('sk.eyJ1Ijoic2lpZGR5IiwiYSI6ImNsOHZvMmlsbzBmbXA0M3F2eWR1cGVsOXIifQ.gkSxce-iibS7ZNGJlrXpXw')
      })
        .on('routesfound', (e) => {
            e.routes[0].coordinates.forEach((coord, index) => {
              setTimeout(() => {
                taxiMarker.setLatLng([coord.lat, coord.lng]);
              }, 100 * index);
            });
          })
        .addTo(map)
    }
  }
})